import React from "react";
import '../assets/css/nucleo-icons.css';
import '../assets/css/nucleo-svg.css';
import '../assets/css/style.css';
import { Link } from 'react-router-dom';


export default function Leftsidebar() {
  return (
      
    <div className="container">
        <aside className="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 " id="sidenav-main">
          <div className="sidenav-header">
            <i className="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
            <Link className="navbar-brand m-0" to={"/"} >
            
              <span className="ms-1 font-weight-bold">Drug Managemnt</span>
            </Link>
          </div>
          
        
          <div className="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link active" to={"/"}>
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-tv-2 text-primary text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">Dashboard</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to={"/drugs/add"}>
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-calendar-grid-58 text-warning text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">Add New Drug</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to={"/drugs/list"}>
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-credit-card text-success text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">View Drugs</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to={"/drugs/list"}>
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-app text-info text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">Expire Drugs</span>
                </Link>
              </li>
             
              <li className="nav-item mt-3">
                <h6 className="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account pages</h6>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to="profile.html">
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-single-02 text-dark text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">Profile</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to="sign-in.html">
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-single-copy-04 text-warning text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">Sign In</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to="sign-up.html">
                  <div className="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="ni ni-collection text-info text-sm opacity-10"></i>
                  </div>
                  <span className="nav-link-text ms-1">Sign Up</span>
                </Link>
              </li>
            </ul>
          </div>
        </aside>
    </div>
  )
}