import React, { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useNavigate, useParams } from 'react-router-dom'
import axios from 'axios';
import Swal from 'sweetalert2';

export default function EditDrugs() {
  const navigate = useNavigate();

  const { id } = useParams()
  const [name, setname] = useState("")
  const [barcode, setbarcode] = useState("")
  const [dossage, setdossage] = useState("")
  const [brand, setbrand] = useState("")
  const [manuDate, setmanuDate] = useState("")
  const [expDate, setexpDate] = useState("")
  const [quantity, setquantity] = useState("")
  const [price, setprice] = useState("")
  const [description, setDescription] = useState("")
  // const [image, setImage] = useState(null)
  const [validationError,setValidationError] = useState({})

  useEffect(()=>{
    fetchDrugs()
  })





    const fetchDrugs = async () => {
    await axios.get(`http://localhost:8000/api/drugs/edit/${id}`).then( res => {

      if(res.data.status === 200)
      {
          setname(res.data.drugs.name)
          setbarcode(res.data.drugs.barcode)
          setdossage(res.data.drugs.dossage)
          setbrand(res.data.drugs.brand)
          setmanuDate(res.data.drugs.manuDate)
          setexpDate(res.data.drugs.expDate)
          setquantity(res.data.drugs.quantity)
          setprice(res.data.drugs.price)
          setDescription(res.data.drugs.description)
      }
  
     });

  }



  const updateDrug = async (e) => {
    e.preventDefault();
    const formData = new FormData()
    formData.append('_method', 'PATCH');
    formData.append('name', name)
    formData.append('barcode', barcode)
    formData.append('dossage', dossage)
    formData.append('brand', brand)
    formData.append('manuDate', manuDate)
    formData.append('expDate', expDate)
    formData.append('quantity', quantity)
    formData.append('price', price)
    formData.append('description', description)
    
    // if(image!==null){
    //   formData.append('image', image)
    // } 

    await axios.post(`http://localhost:8000/api/drugs/${id}`, formData).then(({data})=>{
      Swal.fire({
        icon:"success",
        text:data.message
      })
      navigate("/drugs/list/")
    }).catch(({response})=>{
      if(response.status===422){
        setValidationError(response.data.errors)
      }else{
        Swal.fire({
          text:response.data.message,
          icon:"error"
        })
      }
    })
  }



  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-12 col-sm-12 col-md-6">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Edit Drug</h4>
              <hr />
              <div className="form-wrapper">
                {
                  Object.keys(validationError).length > 0 && (
                    <div className="row">
                      <div className="col-12">
                        <div className="alert alert-danger">
                          <ul className="mb-0">
                            {
                              Object.entries(validationError).map(([key, value])=>(
                                <li key={key}>{value}</li>   
                              ))
                            }
                          </ul>
                        </div>
                      </div>
                    </div>
                  )
                }
                <Form onSubmit={updateDrug}>
                  <Row> 
                      <Col>
                        <Form.Group controlId="name">
                            <Form.Label>name</Form.Label>
                            <Form.Control type="text" value={name} onChange={(event)=>{
                              setname(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="barcode">
                            <Form.Label>Barcode</Form.Label>
                            <Form.Control type="text" value={barcode} onChange={(event)=>{
                              setbarcode(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="dossage">
                            <Form.Label>dossage</Form.Label>
                            <Form.Control type="text" value={dossage} onChange={(event)=>{
                              setdossage(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="brand">
                            <Form.Label>brand</Form.Label>
                            <Form.Control type="text" value={brand} onChange={(event)=>{
                              setbrand(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="manuDate">
                            <Form.Label>Manufactre Date</Form.Label>
                            <Form.Control type="date" value={manuDate} onChange={(event)=>{
                              setmanuDate(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="expDate">
                            <Form.Label>Expire Date</Form.Label>
                            <Form.Control type="date" value={expDate} onChange={(event)=>{
                              setexpDate(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="quantity">
                            <Form.Label>quantity</Form.Label>
                            <Form.Control type="number" min={0} value={quantity} onChange={(event)=>{
                              setquantity(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>

                  <Row> 
                      <Col>
                        <Form.Group controlId="price">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" min={0} value={price} onChange={(event)=>{
                              setprice(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>
                  <Row className="my-3">
                      <Col>
                        <Form.Group controlId="Description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control as="textarea" rows={3} value={description} onChange={(event)=>{
                              setDescription(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>
                  </Row>
                  {/* <Row>
                    <Col>
                      <Form.Group controlId="Image" className="mb-3">
                        <Form.Label>Image</Form.Label>
                        <Form.Control type="file" onChange={changeHandler} />
                      </Form.Group>
                    </Col>
                  </Row> */}
                  <Button variant="primary" className="mt-2" size="lg" block="block" type="submit">
                    Save
                  </Button>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}