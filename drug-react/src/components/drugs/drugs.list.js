import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import Swal from 'sweetalert2'

export default function DrugList() {

    const [drugs, setDrugs] = useState([])

    useEffect(()=>{
        fetchDrugs() 
    })

    const fetchDrugs = async () => {
        await axios.get(`http://localhost:8000/api/drugs`).then(({data})=>{
            setDrugs(data)
            // console.log(data)
        })
    }

    const deleteDrug = async (id) => {
        const isConfirm = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            return result.isConfirmed
          });

          if(!isConfirm){
            return;
          } 

          await axios.delete(`http://localhost:8000/api/drugs/${id}`).then(({data})=>{

          console.log(data)
            Swal.fire({
                icon:"success",
                text:data.message
            })
            fetchDrugs()
          }).catch(({response:{data}})=>{
            Swal.fire({
                text:data.message,
                icon:"error"
            })
          })
    }

    return (
      <div className="container">
          <div className="row">

        <h1 className='text-white text-center'>ACTIVE DRUG LIST</h1>

         <div className="row mt-4">
          <div className="col-lg-12 mb-lg-0 mb-4">
          <div className="card ">
            <div className="card-header pb-0 p-3">
            </div>
            <div className="table-responsive">
              <table className="table align-items-center ">

              <thead>
                <tr className="text-center">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Exp. Date</th>
                    {/* <th>Description</th> */}
                    {/* <th>Image</th> */}
                    <th>Actions</th>
                </tr>
            </thead>

                <tbody>
                {
                drugs.length > 0 && (
                    drugs.map((row, key)=>(
                      row['cnt'] = key,
                        <tr key={key} className="text-center">
                            <td>{row.cnt+1}</td>
                            <td>{row.name}</td>
                            <td>{row.brand}</td>
                            <td>{row.quantity}</td>
                            <td>{row.price}</td>
                            <td>{row.expDate}</td>
                            {/* <td>{row.description}</td> */}
                            <td>
                                {/* <img width="50px" alt='' src={`http://localhost:8000/storage/app/public/product/image/${row.image}`} ></img> */}
                            </td>
                            <td>

                                <Link to={`/drugs/edit/${row.id}`} className='btn btn-success me-2'>
                                    Edit
                                </Link>
                                <Button variant="danger" onClick={()=>deleteDrug(row.id)}>
                                    Delete
                                </Button>
                            </td>
                        </tr>
                    ))
                )
            }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        </div>
    </div>
 </div>

    )
}