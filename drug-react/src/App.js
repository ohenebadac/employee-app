import * as React from "react";
import Container from "react-bootstrap/Container";
import "bootstrap/dist/css/bootstrap.css";
//import "./App.css";

import { BrowserRouter as Router , Routes, Route, Link } from "react-router-dom";
import Leftsidebar from "./components/Layout/Leftsidebar";
import Topnavbar from "./components/Layout/Topnavbar";
import Dashboard from "./components/Layout/Dashboard";
import EditDrugs from "./components/drugs/drugs.edit";
import AddDrug from "./components/drugs/drugs.add";
import DrugList from "./components/drugs/drugs.list";

function App() {
  return (<Router>

  <div className="min-height-300 bg-primary position-absolute w-100"></div>

    <Leftsidebar />

    <main className="main-content position-relative border-radius-lg ">

      <Topnavbar />

      <div className="container-fluid py-4">

       <Container className="mt-5">
  
          <Routes>
            <Route path="/drugs/list" element={<DrugList />} />
            <Route path="/drugs/add" element={<AddDrug />} />
            <Route path="/drugs/edit/:id" element={<EditDrugs />} />
            <Route exact path='/' element={<Dashboard />} />
          </Routes>

      </Container>

      </div>

    </main>

  </Router>);
}

export default App;