<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\Drugs;

class DrugsController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Drugs::select(
            'id',
            'name',
            'barcode',
            'dossage',
            'brand',
            'manuDate', 
            'expDate',
            'quantity',
            'price',
            'description', 
            'image')->get();
    }


    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'barcode'=>'required',
            'dossage'=>'required',
            'brand'=>'required',
            'manuDate'=>'required',
            'expDate'=>'required',
            'quantity'=>'required',
            'price'=>'required',
            'description'=>'required',
            'image'=>'required|image'
        ]);

        try{
            $imageName = Str::random().'.'.$request->image->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('product/image', $request->image,$imageName);
            Drugs::create($request->post()+['image'=>$imageName]);

            return response()->json([
                'message'=>'Product Created Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while creating a product!!'
            ],500);
        }
    }


    public function show(Drugs $drugs)
    {
        return response()->json([
            'drugs'=>$drugs
        ]);
    }


    public function edit($id)
    {
        $drugs = Drugs::find($id);
        if($drugs)
        {
            return response()->json([
                'status'=> 200,
                'drugs' => $drugs,
            ]);
        }
        else
        {
            return response()->json([
                'status'=> 404,
                'message' => 'No drugs ID Found',
            ]);
        }

    }

    public function update(Request $request, $drugid)
    {
        $request->validate([
            'name'=>'required',
            'barcode'=>'required',
            'dossage'=>'required',
            'brand'=>'required',
            'manuDate'=>'required',
            'expDate'=>'required',
            'quantity'=>'required',
            'price'=>'required',
            'description'=>'required'
            // 'image'=>'required|image'
        ]);
     

        try{

            $drugs = Drugs::find($drugid);
            $drugs->fill($request->all());
            // if($request->hasFile('image')){
            //     if($drugs->image){
            //         $exists = Storage::disk('public')->exists("product/image/{$drugs->image}");
            //         if($exists){
            //             Storage::disk('public')->delete("product/image/{$drugs->image}");
            //         }
            //     }

            //     $imageName = Str::random().'.'.$request->image->getClientOriginalExtension();
            //     Storage::disk('public')->putFileAs('product/image', $request->image,$imageName);
            //     $drugs->image = $imageName;
            //     $drugs->save();
            // }

            $drugs->save();
            return response()->json([
                'message'=>'Drug Updated Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while updating a Drug!!'
            ],500);
        }
    }

 
    public function destroy($drug_id)
    {

        try {
            if( Drugs::find($drug_id)->delete()){
                return response()->json([
                    'message'=>'Drug Deleted Successfully!!'
                ]);
            }else{
                return response()->json([
                    'message'=>'coudnt delete'
                ]);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while deleting a drug!!'
            ]);
        }
    }
}
