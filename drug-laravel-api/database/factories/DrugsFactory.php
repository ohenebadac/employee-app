<?php

namespace Database\Factories;
use App\Models\Drugs;
use Illuminate\Database\Eloquent\Factories\Factory;

class DrugsFactory extends Factory
{



    protected $model = Drugs::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'name' => $this->faker->name,
            'barcode' => $this->faker->numberBetween(500000, 1000000),
            'dossage' => $this->faker->randomNumber,
            'brand' => $this->faker->company,
            'manuDate' => $this->faker->date,
            'expDate' => $this->faker->date,
            'quantity' => $this->faker->numberBetween(1,100),
            'price' => $this->faker->numberBetween(1,1000),
            'description' => $this->faker->text,
            'image' => $this->faker->imageUrl(),
         
        ];
    }
}

